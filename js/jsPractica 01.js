 // Función para calcular las pulsaciones promedio
 function calcularPulsacionesPromedio(edad) {
    // Consideramos que cada año tiene 365.25 días
    const diasPorAnio = 365.25;

    // Frecuencia cardíaca máxima teórica
    const frecuenciaMaxima = 220;

    // Calculamos las pulsaciones promedio
    const pulsacionesPromedio = frecuenciaMaxima - edad;

    return pulsacionesPromedio;
  }

  // Función para manejar el clic del botón y mostrar los datos en una tabla
  function calcularPulsaciones() {
    // Obtenemos la edad ingresada por el usuario
    const edad = parseInt(prompt("Ingresa tu edad en años"));

    // Validamos que se haya ingresado un número válido
    if (!isNaN(edad)) {
      // Calculamos las pulsaciones promedio
      const pulsaciones = calcularPulsacionesPromedio(edad);

      // Creamos una fila para la tabla
      const table = document.getElementById("resultadosTabla");
      const newRow = table.insertRow(-1);

      // Insertamos las celdas con los datos
      const ageCell = newRow.insertCell(0);
      const pulsationsCell = newRow.insertCell(1);

      // Agregamos los datos a las celdas
      ageCell.innerHTML = edad;
      pulsationsCell.innerHTML = pulsaciones;
    } else {
      // Mostramos un mensaje de error si la entrada no es válida
      alert("Por favor, ingresa una edad válida");
    }
  }