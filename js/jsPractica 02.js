 // Función para generar un arreglo con valores aleatorios entre 0 y 10
 function generarCalificaciones() {
    const calificaciones = [];

    for (let i = 0; i < 35; i++) {
      // Generamos valores aleatorios entre 0 y 10
      const calificacion = Math.floor(Math.random() * 11);
      calificaciones.push(calificacion);
    }

    return calificaciones;
  }

  // Función para mostrar la información en una tabla
  function mostrarInformacionTabla() {
    // Generamos el arreglo de calificaciones
    const calificaciones = generarCalificaciones();

    // Creamos una fila para la tabla
    const table = document.getElementById("informacionTabla");
    const headerRow = table.insertRow(-1);

    // Insertamos las celdas con los encabezados
    const headerCell1 = headerRow.insertCell(0);
    const headerCell2 = headerRow.insertCell(1);
    const headerCell3 = headerRow.insertCell(2);
    const headerCell4 = headerRow.insertCell(3);

    // Agregamos los encabezados a las celdas
    headerCell1.innerHTML = "Calificaciones";
    headerCell2.innerHTML = "No Aprobados";
    headerCell3.innerHTML = "Aprobados";
    headerCell4.innerHTML = "Promedio General";

    // Creamos una fila para los datos
    const dataRow = table.insertRow(-1);

    // Insertamos las celdas con los datos
    const dataCell1 = dataRow.insertCell(0);
    const dataCell2 = dataRow.insertCell(1);
    const dataCell3 = dataRow.insertCell(2);
    const dataCell4 = dataRow.insertCell(3);

    // Agregamos los datos a las celdas
    dataCell1.innerHTML = calificaciones.join(", ");
    dataCell2.innerHTML = contarNoAprobados(calificaciones);
    dataCell3.innerHTML = contarAprobados(calificaciones);
    dataCell4.innerHTML = calcularPromedioGeneral(calificaciones);
  }

  // Función para contar alumnos no aprobados
  function contarNoAprobados(calificaciones) {
    return calificaciones.filter(calificacion => calificacion < 7).length;
  }

  // Función para contar alumnos aprobados
  function contarAprobados(calificaciones) {
    return calificaciones.filter(calificacion => calificacion >= 7).length;
  }

  // Función para calcular el promedio general
  function calcularPromedioGeneral(calificaciones) {
    const sumaCalificaciones = calificaciones.reduce((acumulador, calificacion) => acumulador + calificacion, 0);
    const promedioGeneral = sumaCalificaciones / calificaciones.length;
    return promedioGeneral.toFixed(1); // Redondeamos a un decimal
  }